## Description

[Nest](https://github.com/nestjs/nest) TypeScript Tasks API.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Running the requests.http

Install [REST Client](https://github.com/nestjs/nest) Visual Studio Code extension.

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## License

Nest is [MIT licensed](LICENSE).
