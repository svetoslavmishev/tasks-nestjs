import {
  ConflictException,
  InternalServerErrorException,
} from "@nestjs/common";
import { EntityRepository, Repository } from "typeorm";
import * as bcrypt from "bcrypt";
import { AuthCredentialsDto } from "./dto/auth-credentials.dto";
import { User } from "./user.entity";

@EntityRepository(User)
export class UsersRepository extends Repository<User> {
  async createUser(authCredentials: AuthCredentialsDto): Promise<void> {
    const { username, password } = authCredentials;

    const salt = bcrypt.genSaltSync(10); // saltRounds
    const hash = bcrypt.hashSync(password, salt);

    const newUser = await this.create({ username, password: hash });

    try {
      await this.save(newUser);
    } catch (error) {
      
      if (error.code === "ER_DUP_ENTRY") {
        throw new ConflictException(`Username ${username} already exists!`);
      } else {
        throw new InternalServerErrorException();
      }
    }
  }
}
