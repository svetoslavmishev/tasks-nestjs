import { createParamDecorator, ExecutionContext } from "@nestjs/common";
import { User } from "./user.entity";

// Extract the entire req object in each route handler
export const GetUser = createParamDecorator(
  (_data: unknown, ctx: ExecutionContext): User => {
    const request = ctx.switchToHttp().getRequest();
    return request.user;
  }
);
