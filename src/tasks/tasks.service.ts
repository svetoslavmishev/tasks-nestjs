import {
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from "@nestjs/common";
import { TaskStatus } from "./task-status.enum";
import { CreateTaskDto } from "./dto/create-task.dto";
import { FilterTaskDto } from "./dto/filter-task.dto";
import { TasksRepository } from "./tasks.repository";
import { InjectRepository } from "@nestjs/typeorm";
import { Task } from "./task.entity";
import { User } from "src/auth/user.entity";

@Injectable()
export class TasksService {
  constructor(
    @InjectRepository(TasksRepository)
    private tasksRepository: TasksRepository
  ) {}

  async getAllTasks(): Promise<Task[]> {
    return this.tasksRepository.find({
      order: { createdDate: "DESC" },
    });
  }

  async getFilteredTasks(filterTaskDto: FilterTaskDto): Promise<Task[]> {
    const { status, search } = filterTaskDto;
    let tasks = await this.getAllTasks();

    if (status) {
      tasks = tasks.filter((task) => task.status === status);
    }

    if (search) {
      tasks = tasks.filter((task) =>
        task.title.includes(search) || task.description.includes(search)
          ? true
          : false
      );
    }

    return tasks;
  }

  async createTask(createTaskDto: CreateTaskDto, user: User): Promise<Task> {
    const { title, description } = createTaskDto;
    const task = this.tasksRepository.create({
      title,
      description,
      status: TaskStatus.OPEN,
      user
    });

    try {
      await this.tasksRepository.save(task);
    } catch (error) {
      console.log(error);
      throw new InternalServerErrorException();
    }

    return task;
  }

  async getTaskById(id: string): Promise<Task> {
    const task = await this.tasksRepository.findOne(id);

    if (!task) {
      throw new NotFoundException(
        `Task with ID:${id} not found!`,
        "NotFoundException"
      );
    }

    return task;
  }

  async updateTaskStatus(id: string, status: TaskStatus): Promise<Task> {
    const task = await this.getTaskById(id);
    task.status = status;

    try {
      await this.tasksRepository.save(task);
    } catch (error) {
      console.log(error);
      throw new InternalServerErrorException();
    }
    return task;
  }

  async deleteTask(id: string): Promise<Task> {
    const task = await this.getTaskById(id);

    try {
      await this.tasksRepository.remove(task);
    } catch (error) {
      console.log(error);
      throw new InternalServerErrorException();
    }
    return task;
  }
}
